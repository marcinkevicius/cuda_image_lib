#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/core/cuda_stream_accessor.hpp>

#include <chrono>

using namespace cv;
using namespace std;


//__global__ void myKernel(const cv::cuda::PtrStepSzf input, cv::cuda::PtrStepSzf output)
__global__ void myKernel(const cv::cuda::PtrStepSzb input, cv::cuda::PtrStepSzb output)
    {
        int x = blockIdx.x * blockDim.x + threadIdx.x;
        int y = blockIdx.y * blockDim.y + threadIdx.y;

        //unsigned char tmp_val;

        if (x <= input.cols - 1 && y <= input.rows - 1 && y >= 0 && x >= 0)
        {

//              if (input(y, x) - 20 < 0){
//                output(y, x) = input(y, x)-20;
//                if (x < 16 && y < 16)
//                      printf("size %d, val: %d, new_val: %d", sizeof(output(y, x)), input(y, x), input(y, x) -20 );
//              } else {
//                output(y, x) = input(y, x);
//                if (x < 16 && y < 16){
//                      //printf("size of wxh -> %d X %d, sizeof %d", input.cols, input.rows, (output(y, x)));
//                                      //printf("size %d\n", sizeof(output(y, x)));
//                      printf("x1 %d, x2 %d, x3 %d, \n", output(y, x), output(y+1, x), output(y+2, x));
//                      output(y, x) = 255;
//                }
//
//              }
                output(y, x) = 0;
        }
    }

__global__ void bgr_to_abgr_kernel(const cv::cuda::PtrStepSzb input, cv::cuda::PtrStepSzb output)
{
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;

	if (x < input.cols && y < input.rows)
	{
//		output(y, x*4) = (uint8_t) 0;
//		output(y, x*4+1) =  input(y,x*3);
//		output(y, x*4+2) =  input(y,x*3+1);
//		output(y, x*4+3) =  input(y,x*3+2);

		output(y, x*4) = input(y,x*3+2);
		output(y, x*4+1) = input(y,x*3+1);
		output(y, x*4+2) = input(y,x*3);
		output(y, x*4+3) = (uint8_t) 0;
	}
}

__device__ float getRadialX(float x, float y, int centerX, int centerY, float K)
{
	return x + ((x - centerX) * K * ((x - centerX) * (x - centerX) + (y - centerY) * (y - centerY)));
}

__device__ float getRadialY(float x, float y, int centerX, int centerY, float K)
{
	return y + ((y - centerY) * K * ((x - centerX) * (x - centerX) + (y - centerY) * (y - centerY)));
}

__device__ void sampleImageTest(const cv::cuda::PtrStepSzb &input, float idx0, float idx1, uchar3& result, int width, int height)
{
	// if one of index is out-of-bound
	if((idx0 < 0) ||
			(idx1 < 0) ||
			(idx0 > height - 1) ||
			(idx1 > width - 1))
	{
			//temp = Scalar(0, 0, 0, 0);
			result.x = 0;
			result.y = 0;
			result.z = 0;
			//result.val[3] = 0;
			return;
	}

	int idx0_floor = (int)floor(idx0);
	int idx0_ceil = (int)ceil(idx0);
	int idx1_floor = (int)floor(idx1);
	int idx1_ceil = (int)ceil(idx1);

	uchar s1r = input(idx0_floor, idx1_floor*3);
	uchar s1g = input(idx0_floor, idx1_floor*3+1);
	uchar s1b = input(idx0_floor, idx1_floor*3+2);

	uchar s2r = input(idx0_floor, idx1_ceil*3);
	uchar s2g = input(idx0_floor, idx1_ceil*3+1);
	uchar s2b = input(idx0_floor, idx1_ceil*3+2);

	uchar s3r = input(idx0_ceil, idx1_ceil*3);
	uchar s3g = input(idx0_ceil, idx1_ceil*3+1);
	uchar s3b = input(idx0_ceil, idx1_ceil*3+2);

	uchar s4r = input(idx0_ceil, idx1_floor*3);
	uchar s4g = input(idx0_ceil, idx1_floor*3+1);
	uchar s4b = input(idx0_ceil, idx1_floor*3+2);

	float x = idx0 - idx0_floor;
	float y = idx1 - idx1_floor;

	result.x = s1r * (1 - x) * (1 - y) + s2r * (1 - x) * y + s3r * x * y + s4r * x * (1 - y);
	result.y = s1g * (1 - x) * (1 - y) + s2g * (1 - x) * y + s3g * x * y + s4g * x * (1 - y);
	result.z = s1b * (1 - x) * (1 - y) + s2b * (1 - x) * y + s3b * x * y + s4b * x * (1 - y);
}


__global__ void barrel_distort_kernel(const cv::cuda::PtrStepSzb input, cv::cuda::PtrStepSzb output, cv::cuda::PtrStepSzf koef)
{
        float centerX = input.cols / 2;
        float centerY = input.rows / 2;
        //float k = *koef;

        for(int j = blockIdx.x * blockDim.x + threadIdx.x; j < input.rows; j += blockDim.x * gridDim.x)
        {
                for(int i = blockIdx.y * blockDim.y + threadIdx.y; i < input.cols; i += blockDim.y * gridDim.y)
                {
                        uchar3 temp;
                        float x = getRadialX((float)i, (float)j, centerX, centerY, koef(0,0));
                        float y = getRadialY((float)i, (float)j, centerX, centerY, koef(0,0));
                        sampleImageTest(input, y, x, temp, input.cols, input.rows);

                        output(j, (i*3)) = temp.x;
                        output(j, (i*3+1)) = temp.y;
                        output(j, (i*3+2)) = temp.z;
                }
        }
}

__global__ void barrel_distort_abgr_kernel(const cv::cuda::PtrStepSzb input, cv::cuda::PtrStepSzb output, cv::cuda::PtrStepSzf koef)
{
        float centerX = input.cols / 2;
        float centerY = input.rows / 2;
        //float k = *koef;

        for(int j = blockIdx.x * blockDim.x + threadIdx.x; j < input.rows; j += blockDim.x * gridDim.x)
        {
                for(int i = blockIdx.y * blockDim.y + threadIdx.y; i < input.cols; i += blockDim.y * gridDim.y)
                {
                        uchar3 temp;
                        float x = getRadialX((float)i, (float)j, centerX, centerY, koef(0,0));
                        float y = getRadialY((float)i, (float)j, centerX, centerY, koef(0,0));
                        sampleImageTest(input, y, x, temp, input.cols, input.rows);

                        output(j, (i*4)) = temp.z;
                        output(j, (i*4+1)) = temp.y;
                        output(j, (i*4+2)) = temp.x;
                        output(j, (i*4+3)) = (uint8_t) 0;
                }
        }
}

void barrelDistort(cv::InputArray _input,
                    cv::OutputArray _output,
                    cv::InputArray _koef,
                    cv::cuda::Stream _stream)
    {
//		printf("start\n"); std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        const cv::cuda::GpuMat input = _input.getGpuMat();
        const cv::cuda::GpuMat koef = _koef.getGpuMat();

        //_output.create(input.size(), input.type());
        cv::cuda::GpuMat output = _output.getGpuMat();

        dim3 cthreads(32, 32);
        dim3 cblocks(
            static_cast<int>(std::ceil(input.size().width / static_cast<double>(cthreads.x))),
            static_cast<int>(std::ceil(input.size().height / static_cast<double>(cthreads.y))));

        cudaStream_t stream = cv::cuda::StreamAccessor::getStream(_stream);
        //myKernel<<<cblocks, cthreads, 0, stream>>>(input, output);

        barrel_distort_kernel<<<cblocks, cthreads, 0, stream>>>(input, output, koef);
        cudaStreamSynchronize(stream);
//        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
//        std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]" << std::endl;
     //   cudaSafeCall(cudaGetLastError());
    }

void barrelDistortABGR(cv::InputArray _input,
                    cv::OutputArray _output,
                    cv::InputArray _koef,
                    cv::cuda::Stream _stream)
    {
//		printf("start\n"); std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        const cv::cuda::GpuMat input = _input.getGpuMat();
        const cv::cuda::GpuMat koef = _koef.getGpuMat();

        //_output.create(input.size(), input.type());
        cv::cuda::GpuMat output = _output.getGpuMat();

        dim3 cthreads(32, 32);
        dim3 cblocks(
            static_cast<int>(std::ceil(input.size().width / static_cast<double>(cthreads.x))),
            static_cast<int>(std::ceil(input.size().height / static_cast<double>(cthreads.y))));

        cudaStream_t stream = cv::cuda::StreamAccessor::getStream(_stream);
        //myKernel<<<cblocks, cthreads, 0, stream>>>(input, output);

        barrel_distort_abgr_kernel<<<cblocks, cthreads, 0, stream>>>(input, output, koef);
        cudaStreamSynchronize(stream);
//        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
//        std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]" << std::endl;
     //   cudaSafeCall(cudaGetLastError());
    }


void bgr_to_abgr(cv::InputArray _input,
                    cv::OutputArray _output,
                    cv::cuda::Stream _stream)
    {
//	printf("start cvcolor\n"); std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        const cv::cuda::GpuMat input = _input.getGpuMat();

        //_output.create(input.size(), input.type());
        cv::cuda::GpuMat output = _output.getGpuMat();

        dim3 cthreads(32, 32);
        dim3 cblocks(
            static_cast<int>(std::ceil(input.size().width / static_cast<double>(cthreads.x))),
            static_cast<int>(std::ceil(input.size().height / static_cast<double>(cthreads.y))));

        cudaStream_t stream = cv::cuda::StreamAccessor::getStream(_stream);


        bgr_to_abgr_kernel<<<cblocks, cthreads, 0, stream>>>(input, output);
        cudaStreamSynchronize(stream);
//        cudaDeviceSynchronize();
//        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
//        std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]" << std::endl;
    }
