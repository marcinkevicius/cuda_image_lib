//============================================================================
// Name        : firstCudaHW.cpp
// Author      : Andrius
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/cudev/ptr2d/gpumat.hpp>

using namespace cv;
using namespace std;

void barrelDistort(cv::InputArray _input, cv::OutputArray _output, cv::InputArray _koef, cv::cuda::Stream _stream);
void bgr_to_abgr(cv::InputArray _input, cv::OutputArray _output, cv::cuda::Stream _stream);
void barrelDistortABGR(cv::InputArray _input, cv::OutputArray _output, cv::InputArray _koef, cv::cuda::Stream _stream);

int main(  ) {
        int somevar = 0;
        std::cout << "!!!Hello World!!!" << std::endl; // prints !!!Hello World!!!
        std::cout << somevar << std::endl;

//      cv::String imageName( "gpuT.png" ); // by default
//      if( argc > 1)
//      {
//              imageName = argv[1];
//      }
        cv::Mat image;

//        cv::Mat image3;
//        cv::Mat abgr_cvmat;
//        cv::cuda::GpuMat abgr_gpumat;
        image = cv::imread("/home/kid/Desktop/R.png", IMREAD_COLOR ); // Read the file
        if( image.empty() )                      // Check for invalid input
        {
                std::cout <<  "Could not open or find the image" << std::endl ;
                return -1;
        }

//        cv::Mat image2;

//        cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE ); // Create a window for display.
//        cv::imshow( "Display window", image );                // Show our image inside it.
//        cv::waitKey(0); // Wait for a keystroke in the window

        cv::cuda::GpuMat d1_GpuUC3(image);
        cv::cuda::GpuMat d2_GpuUC3(image);
        cv::cuda::GpuMat d1_GpuUC4(image.rows, image.cols, CV_8UC4, Scalar(255,255, 255, 255));

//        cv::cvtColor(image, abgr_cvmat, COLOR_BGR2BGRA);
//        abgr_gpumat.upload(abgr_cvmat);
//        printf("step uc4 %d", abgr_cvmat.step);

//        cv::cuda::GpuMat d_dst(image);
//        cv::cuda::GpuMat abgr(image.rows, image.cols, CV_8UC4, Scalar(255,255, 255, 255));

        cv::Mat h_Koef(1, 1, CV_32FC1);
        h_Koef.at<float>(0,0) = 0.0000005;
        cv::cuda::GpuMat d_koef(h_Koef);

		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
		barrelDistort(d1_GpuUC3, d2_GpuUC3, d_koef, cv::cuda::Stream());
        bgr_to_abgr(d2_GpuUC3, d1_GpuUC4, cv::cuda::Stream());
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		std::cout << "Time difference 1,1 = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]" << std::endl;

		d1_GpuUC4.download(image);
		cv::imshow( "Display window", image );
		cv::waitKey(0);

//		begin = std::chrono::steady_clock::now();
//		barrelDistortABGR(d1_GpuUC3, d1_GpuUC4, d_koef, cv::cuda::Stream());
//		end = std::chrono::steady_clock::now();
//		std::cout << "Time difference 1+1 = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]" << std::endl;



//        cv::waitKey(0);
//
//        printf("step uc4 %d", abgr_gpumat.step);
//        //abgr.step = 4;

        d1_GpuUC4.download(image);
//        printf("step uc4 %d", abgr_gpumat.step);

//        unsigned char * p = image2.data;
//        uchar *p2 = abgr_cvmat.data;
//        printf("abgr cuda %d, %d, %d, %d | %d, %d, %d, %d | %d, %d, %d, %d | %d, %d, %d, %d |- %d\n",
//        		p[0],p[1],p[2],p[3],p[4],p[5],p[6],p[7],p[8],p[9],p[10],p[11],p[12],p[13],p[14],p[15],p[16]);
//        printf("abgrcvmat %d, %d, %d, %d | %d, %d, %d, %d | %d, %d, %d, %d | %d, %d, %d, %d |- %d\n",
//        		p2[0],p2[1],p2[2],p2[3],p2[4],p2[5],p2[6],p2[7],p2[8],p2[9],p2[10],p2[11],p2[12],p2[13],p2[14],p2[15],p2[16]);
//
//        printf("cv 8uc4 type %d got %d", image.type(), image2.type());
        cv::imshow( "Display window", image );                // Show our image inside it.

        cv::waitKey(0); // Wait for a keystroke in the window

        return 0;
}
